﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tasl27.Models
{
    public class SupervisorsContext: DbContext
    {
        public SupervisorsContext(DbContextOptions<SupervisorsContext> options) : base(options)
        {

        }

        public DbSet<Supervisor> Supervisors { get; set; }
    }
}
